# Projet Tiny Amazon

### A- Description du projet

Le projet Tiny Amazon a été proposer par l'IUT d'Orleans.

### B- But du projet

Ce projet vise à nous faire apprendre comment utiliser le framework web "Flask".

### C- Comment lancer le serveur ?
    git clone https://gitlab.com/cookiehacker/rockproject_2a
    virtualenv -p python3 venv
    source venv/bin/activate
    pip install -r rockproject_2a/projet/requirements.txt
    cd rockproject_2a/projet/rockproject
    flask loaddb bd.yaml
    flask newuser id mdp
    flask run

URL : http://127.0.0.1:5000

### D- Liste des développeurs

- Erwan AUBRY
- Julien DELAUNAY
- Marion JURÈ

### E- La norme flow

La norme flow fonctionne en plusieurs étape :

- Cloner le projet
- Creer une branche pour votre nouvelle fonctionnalité
- Faites un pull request via gitlab
- En parler via notre serveur discord
- Attendre que nous faisions le necessaire

### F- Comment utiliser la norme FLOW ?

La norme FLOW est basé sur le fonctionnement du logiciel git-flow. Il fonctionne sur le principe de creer une branche lors de l'ajout d'une fonctionnalité. Ce qui permet de séparer chaque ajout de fonctionnalité et donc d'une meilleure gestion des conflits entre les branches.

Dans le git on trouvera deux branches principales, une "master" une "develop".
- master sera utiliser lors de la sortie d'une nouvelle version (Alpha, Beta, V1, V2, ...)
- develop sera la branche où chaqu'un fera un "merge request" de leur branche.
  
Un "merge request" est une fonctionnalité de GitLab permettant de demander l'autorisation aux administrateur du GIT si oui ou non on autorise la fusion de votre branche au develop.

**ATTENTION ! Ne jamais push sur MASTER, toutes vos modifs ce feront sur vos branches puis seront envoyés sur la branche "develop" et quand la version jugé fini, la branche "develop" et "master" seront fusionées !"**

#### 1- Creation d'une branche

    git branch nomDeMaFonctionnalite

#### 2- Changement de branche

    git checkout nomBranche

#### 3- Verification de la branche où on est

    git status

#### 4- Récuperation des modifications

    git add .

#### 5- Realisation d'un commit
Un commit doit être sous la forme de ceci : 

    {feat | refact | bugfix | style}{(nomFichier)}:{description}

Où :
- feat désigne l'ajout d'une fonctionnalité
- refact désigne la modification d'un code sans réel conséquence
- bugfix désigne la correction d'un bug
- style désigne l'amélioreration de l'estétique

#### 6- Commit

    git commit -m "feat (fichier) : développement de la partie inscription"

#### 7- Envoie du commit

    git push origin nomDeLaBranche

#### 8- Envoie d'un merge request

Allez sur le site web de GitLab ainsi que dans le projet.
Faites :
- clique sur Demande de fusion
- New "merge request"
- Dans "source branch" selectionner votre branche
- Dans "targer branch" selectionnez develop
- Cliquez sur "compare branch and continue"
- Les champs sont facultatifs donc descendez et cliquez sur le bouton "submit demande de fusion"
  
#### 9- Supprimer la branche en locale

    git branch -D nomDeLaBranche

### G- Logiciels utilisés

- VSCode
- GIT
- Flask
- SQLite

