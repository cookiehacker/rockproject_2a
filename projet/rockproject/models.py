from .app import db, login_manager
from flask_login import UserMixin

'''
Liaison en many to many entre la table MUSIQUE et la table GENRE

MUSIQUE | 1,n ----------------- 1,n | GENRE
'''
appartientAuGenre = db.Table(
    'appartientAuGenre',
    db.metadata,
    db.Column('genre_id', db.Integer, db.ForeignKey('GENRE.id'), primary_key=True),
    db.Column('music_id', db.Integer, db.ForeignKey('MUSIQUE.id'), primary_key=True)
)

'''
Liaison en many to many entre la table user et la table GROUPE

user | 1,n ----------------- 1,n | GROUPE
'''
estfandugroupe = db.Table(
    'estfandugroupe',
    db.metadata,
    db.Column('user_u', db.String(50), db.ForeignKey('user.username'), primary_key=True),
    db.Column('group_id', db.Integer, db.ForeignKey('GROUPE.id'), primary_key=True)
)

'''
Liaison en many to many entre la table user et la table music

user | 1,n ----------------- 1,n | MUSIQUE
'''
estfandemusic = db.Table(
    'estfandemusic',
    db.metadata,
    db.Column('user_u', db.String(50), db.ForeignKey('user.username'), primary_key=True),
    db.Column('music_id', db.Integer, db.ForeignKey('MUSIQUE.id'), primary_key=True)
)

'''
La table ARTISTE

Attributs :
    id = L'identifiant d'un artiste, un entier qui est une clef primaire
    name = Nom de l'artiste qui est une chaine de caractére de 200 caractéres max
'''
class Artist(db.Model):
    __tablename__ = 'ARTISTE'
    id          = db.Column(db.Integer, primary_key=True)
    name        = db.Column(db.String(100))

    def __repr__(self):
        return "<Artiste (%d) %s>" % (self.id, self.name)

'''
La table GROUPE

Attributs :
    id = L'identifiant d'un groupe, un entier qui est une clef primaire
    bandName = Nom du groupe qui est une chaine de caractére de 100 caractéres max

Relation :
    Relation entre la table GROUPE et ARTISTE
'''
class Band(db.Model):
    __tablename__ = 'GROUPE'
    id          = db.Column(db.Integer, primary_key=True)
    bandName    = db.Column(db.String(100))
    artist_id   = db.Column(db.Integer, db.ForeignKey("ARTISTE.id"))
    artist      = db.relationship("Artist", backref=db.backref("GROUPE", lazy="dynamic"))
    users       = db.relationship('User', secondary=estfandugroupe, lazy='subquery')

    def __repr__(self):
        return "<Band (%d) %s>" % (self.id, self.bandName)

'''
La table MUSIQUE

Attributs :
    id = L'identifiant d'une musique, un entier qui est une clef primaire
    title = titre d'une musique qui est une chaine de caractére de 100 caractéres max
    year = Annee d'une musique qui est un entier
    img = Image de couverture d'une musique qui est une chaine de caractere de 500 caratere max

Relation :
    Relation entre la table MUSIQUE et GROUPE
    Relation entre la table MUSIQUE et GENRE
'''
class Music(db.Model):
    __tablename__ = 'MUSIQUE'
    id          = db.Column(db.Integer, primary_key=True)
    title       = db.Column(db.String(100))
    year        = db.Column(db.Integer)
    img         = db.Column(db.String(500))
    band_id     = db.Column(db.Integer, db.ForeignKey("GROUPE.id"))
    band        = db.relationship("Band", backref=db.backref("MUSIQUE", lazy="dynamic"))
    genres      = db.relationship('Genre', secondary=appartientAuGenre, lazy='subquery')
    users       = db.relationship('User', secondary=estfandemusic, lazy='subquery')
    def __repr__(self):
        return "<Music (%d) %s>" % (self.id, self.title)

'''
La table GENRE

Attributs :
    id = L'identifiant d'un genre, un entier qui est une clef primaire
    genreName = nom d'un genre qui est une chaine de caractére de 100 caractéres max

Relation :
    Relation entre la table GENRE et MUSIQUE
'''
class Genre(db.Model):
    __tablename__ = 'GENRE'
    id          = db.Column(db.Integer, primary_key=True)
    genreName   = db.Column(db.String(100))
    musics      = db.relationship('Music', secondary=appartientAuGenre, lazy='subquery')

'''
La table USER

Attributs :
    username = pseudo de l'utilisateur qui est une chaine de caractére de 50 caractéres max
    password = mot de passe de l'utilisateur qui est une chaine de caractére de 64 caractéres max

'''
class User(db.Model, UserMixin):
    username    = db.Column(db.String(50), primary_key = True)
    password    = db.Column(db.String(64))
    groupes     = db.relationship('Band', secondary=estfandugroupe, lazy='subquery')
    musics    = db.relationship('Music', secondary=estfandemusic, lazy='subquery')
    '''
    Methode permettant de recuperer l'ID
    '''
    def get_id(self):
        return self.username

'''
Methode permettant la recuperation de tout les genres d'une musique
'''
def getGenreDeLaMusique(mus):
    m = db.session.query(Music).filter(Music.id == mus).all()
    res = []
    for musique in m:
        for g in musique.genres:
            res.append(g.genreName)
    return res

'''
Methode permettant la recuperation de toutes les musiques d'un genre
'''
def getMusiqueDuGenre(ge):
    g = db.session.query(Genre).filter(Genre.id == ge).all()
    res = []
    for genre in g:
        for m in genre.musics:
            res.append(m.title)
    return res

'''
Recuperation des groupe favories d'un utilisateur
'''
def getGroupFavorieDuUser(us):
    u = db.session.query(User).filter(User.username == us).all()
    res = []
    for g in u.groupes:
        res.append(g.id)
    return res

'''
Methode permettant la recuperation des artists sous forme compatible avec un type de 
formulaire : QuerySelectField
'''
def artist_query():
    return Artist.query

'''
Methode permettant la recuperation des groupes sous forme compatible avec un type de 
formulaire : QuerySelectField
'''
def band_query():
    return Band.query

'''
Methode permettant la recuperation des genres sous forme de query
'''
def genre_query():
    return Genre.query

'''
Methode permettant la recuperation de tout les groupes
'''
def getAllGroup():
    return db.session.query(Band).all()

'''
Methode permettant la recuperation d'un groupe via son ID
'''
def getGroup(id):
    return Band.query.get(id)

'''
Methode permettant la suppression d'un groupe via son ID
'''
def removeGroup(id):
    Band.query.filter(Band.id == id).delete()

'''
Methode permettant la recuperation de tout les artistes
'''
def getAllArtist():
    return db.session.query(Artist).all()

'''
Methode permettant la recuperation d'un artiste via son ID
'''
def getArtist(id):
    return Artist.query.get(id)

'''
Methode permettant la suppression d'un artiste via son ID
'''
def removeArtist(id):
    Artist.query.filter(Artist.id == id).delete()

'''
Chargement d'un utilisateur via son pseudo
'''
@login_manager.user_loader
def load_user(username):
    return User.query.get(username)


'''

Methode permettant la recuperation une musique à partir d'un id
'''    
def get_music(id):
	return  Music.query.get(id)


'''
Methode permettant la recuperation le genre à partir d'un id
'''    	
def get_genre(id):
	return  Genre.query.get(id)
	
'''
Methode permettant la recuperation le genre à partir d'un id
'''    	
def get_band(id):
	return  Band.query.get(id)	

'''
Methode permettant la recuperation tous les groupes avec leurs ids et leurs noms  
''' 
def all_band():
	try:
		res = Band.query.all()
		liste=[]
		for i in range(len(res)):
			liste.append((res[i].id,res[i].bandName))
		return liste
	except :
		return []

'''
Methode permettant la recuperation tous les genres avec leurs ids et leurs noms  
''' 
def all_genre():
    try:
        res =Genre.query.all()
        liste=[]
        for i in range(len(res)):
            liste.append((res[i].id,res[i].genreName))
        return liste
    except :
        return []


'''
Methode permettant la recuperation toutes les musiques  
''' 
def all_music():
	return Music.query.all()


'''
Methode permettant la recuperation toutes les musiques qui sont triées par année   
''' 
def tri_year_music():
	return Music.query.order_by(Music.year).all()


'''
Methode permettant la recuperation toutes les musiques qui sont triées par groupe   
''' 
def tri_band_music():
	groupe = Band.query.order_by(Band.bandName).all()
	liste = []
	for i in groupe:
		music = get_groupe_Musique(i.id)
		for m in music:
			liste.append(m) 
	return liste

'''
Methode permettant la recuperation toutes les musiques qui sont triées par titre   
''' 
def tri_title_music():
	return Music.query.order_by(Music.title).all()  
  
'''
Methode permettant la recuperation toutes les musiques d'un groupe  
''' 
def get_groupe_Musique(idGroupe):
	return Music.query.filter(Music.band_id == idGroupe).all()

'''
Methode permettant la recuperation de toutes les musiques d'un genre
'''
def get_genre_Musique(ge):
    g = db.session.query(Genre).filter(Genre.id == ge).all()
    res = []
    for genre in g:
        for m in genre.musics:
            res.append(m)
    return res

'''
Methode permettant la recuperation toutes les musiques qui sont triées par genre   
''' 
def tri_genre_music():
	genre= Genre.query.order_by(Genre.genreName).all()
	liste=[]
	for i in genre:
		music=get_genre_Musique(i.id)
		for m in music:
			liste.append(m) 
	return liste

'''
Methode permettant la recuperation id d'une musique à partir d'un titre   
''' 
def get_un_musique(titre):
	m=Music.query.filter(Music. title == titre).all()
	if len(m)==0:
		return 0
	return m[0]

'''
Methode donnant la liste des 10eme musiques les plus aiment par tous les utilisateurs  
''' 
def get_music_prefere():
	def critere(clee):
		return dico[clee]
	users=User.query.all()
	dico={}
	for user in users:
		print(user.musics)
		for m in user.musics:
			if m not in dico:
				dico[m]=0
			dico[m]+=1
	liste=sorted(dico,key=critere,reverse=True)
	if len(liste)<10:
		return liste
	return liste[:10]
