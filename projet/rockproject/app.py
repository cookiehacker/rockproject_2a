#! /usr/bin/env python3
from flask import Flask
from flask_script import Manager
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
import os.path


def mkpath(p):
    return os.path.normpath(
        os.path.join(
            os.path.dirname(__file__), p
        )
    )

app = Flask(__name__)
login_manager = LoginManager(app)
login_manager.login_view = "login"

app.debug = True
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('../BD.db'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

app.config['SECRET_KEY'] = "d55b1c41-2951-4019-9a53-a947f5a8e6d0"



bootstrap = Bootstrap(app)
db = SQLAlchemy(app)




