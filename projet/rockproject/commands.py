import click
from .app import app, db


@app.cli.command()
@click.argument('filename')
def loaddb(filename):
    '''
    Chargement de la base de données
    '''
    db.create_all()

    import yaml
    musics = yaml.load(open(filename))

    from .models import Artist, Band, Music, Genre

    # Recuperation artiste
    artist = {}
    for m in musics:
        a = m["parent"]
        if a not in artist:
            o = Artist(name = a)
            db.session.add(o)
            artist[a] = o
    db.session.commit()

    # Recuperation groupe
    bands = {}
    for m in musics:
        g = m["by"]
        a = artist[m["parent"]]
        if g not in bands:
            o = Band(bandName = g, 
                     artist_id = a.id)
            db.session.add(o)
            bands[g] = o
    db.session.commit()

    # Recuperation musique
    music = {}
    for m in musics:
        g = bands[m["by"]]
        entry = m["entryId"]
        if entry not in music:
            o = Music(  title = m["title"],
                        year = m["releaseYear"],
                        img = m["img"],
                        band_id = g.id)
            db.session.add(o)
            music[entry] = o
    db.session.commit()

    # Recuperation genre 
    genres = []
    lsObjGenre = []
    for m in musics:
        mus = music[m["entryId"]]
        for genre in m["genre"]:
            if genre not in genres:
                o = Genre(genreName = genre)
                lsObjGenre.append(o)
                db.session.add(o)
                genres.append(genre)
    db.session.commit()

    # Liaison musique à genre
    for m in musics:
        mus = music[m["entryId"]]
        for g in m["genre"]:
            for og in lsObjGenre:
                if g == og.genreName:
                    mus.genres.append(og)
        db.session.commit()
    
    # Liaison genre à musique
    for m in musics:
        mus = music[m["entryId"]]
        for g in lsObjGenre:
            if g.genreName in m["genre"]:
                g.musics.append(mus)
        db.session.commit()

@app.cli.command()
def syncdb():
    '''
    Synchronise la BD
    '''
    db.create_all()

@app.cli.command()
@click.argument('username')
@click.argument('password')
def newuser(username, password):
    '''
    Ajout d'un utilisateur
    '''
    from .models import User
    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    u = User(username = username, password = m.hexdigest())
    db.session.add(u)
    db.session.commit()
