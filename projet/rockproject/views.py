#! /usr/bin/env python3

from .app import app,db,mkpath
from flask import render_template,url_for,redirect
from .app import app, db
from flask import render_template, url_for, redirect, request

# IMPORTATION DES MODELS
from .models import (
	get_music_prefere,
	get_un_musique,
	tri_genre_music,
	tri_title_music,
	tri_band_music,
	tri_year_music,
	all_music,
	get_band,
	get_genre,
	appartientAuGenre,
	all_genre,
	all_band,
	Music,
	get_music,
    getGenreDeLaMusique, 
    getMusiqueDuGenre, 
    artist_query, 
    Band,
    getGroup,
    getAllGroup,
    removeGroup,
    User,
    Artist,
    getAllArtist,
    getArtist,
    removeArtist,
    band_query,
)

# IMPORTATION POUR FORMULAIRE
from flask_wtf import FlaskForm
from wtforms import *
from wtforms.validators import DataRequired,InputRequired,Required
from flask_wtf.file import FileField, FileRequired
from wtforms.ext.sqlalchemy.fields import QuerySelectField

# LOGIN
from hashlib import sha256
from flask_login import (
    login_user, 
    current_user, 
    logout_user, 
    login_required
)

from werkzeug.utils import secure_filename


'''
Formulaire de saisie du groupe

Attribut :
    id = Un champs caché prenant l'ID du groupe
    name = Un champs de texte obligatoire destiner à la saisie du nom du groupe
    artist = menu deroulant listant tout les nom des artists presant dans la base de donnée et designable
             comme fondateur du groupe.
'''
class GroupForm(FlaskForm):
    id      = HiddenField('id')
    name    = StringField("Nom", validators=[DataRequired()])
    artist  = QuerySelectField("L'artiste fondateur : ",query_factory=artist_query, allow_blank=True, get_label="name")

'''
Formulaire permettant de garder l'ID lors de la suppression d'un groupe

Attribut:
    id = Un champs caché prenant l'ID du groupe
'''
class GroupFormDelete(FlaskForm):
    id      = HiddenField('id')

'''
Formulaire de saisie ou modification d'un artiste

Attribut :
    id = Un champs caché prenant l'ID de l'artiste
    name = Un champs de texte obligatoire destiner à la saisie du nom de l'artiste
'''
class ArtistForm(FlaskForm):
    id      = HiddenField('id')
    name    = StringField('Nom', validators=[DataRequired()])

'''
Formulaire permettant de garder l'ID lors de la suppression d'un artiste

Attribut:
    id = Un champs caché prenant l'ID de l'artiste
'''
class ArtistFormDelete(FlaskForm):
    id      = HiddenField('id')

'''
Formulaire de connexion

Attribut:
    username = Un champs de texte destiner à la saisie du pseudo de l'utilisateur
    password = Un champs de texte destiner à la saisie du mot de passe de l'utilisateur
    next = Une page au cas où l'utilisateur essaye d'aller sur une fonctionnalité reserver aux personne connecter avant de ce connecter
'''
class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    next = HiddenField()
    '''
	Verification de l'authentification de l'utilisateur
    '''
    def get_authenticated_user(self):
        user = User.query.get(self.username.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None

'''
Multiple de case à cocher

Attribut :
    widget = la liste des noms
    option_widget = la liste des cases à cocher
'''
class MultiCheckboxField(SelectMultipleField):
	widget = widgets.ListWidget(prefix_label=False)
	option_widget = widgets.CheckboxInput()


'''
Formulaire de saisie de la musique

Attribut :
    id = Un champs caché prenant l'ID de la musique
    title = Un champs de texte obligatoire destiner à la saisie le titre de la musique
    year = Un champs de texte obligatoire destiner à la saisie l'année de sortie de la musique
    fileName = bouton permettant insection d'une image
    band = menu deroulant listant tout les nom des groupes presant dans la base de donnée
    genre= checkbox listant tous les genres
    submit= bonton pour valider le formulaire
'''
class MusicForm(FlaskForm):
	id = HiddenField('id')
	title = StringField('Titre',validators=[DataRequired()])
	year = IntegerField('Année de publication',validators=[DataRequired()])
	fileName = FileField('Image',validators=[FileRequired()])
	band = SelectField('Groupe',coerce=int, choices=all_band(),validators=[DataRequired()])
	genre = MultiCheckboxField('Genre',coerce=int, choices=all_genre())
	submit = SubmitField("Enregistrer")

'''
Formulaire de saisie de la musique sans insection d'une image

Attribut :
    id = Un champs caché prenant l'ID de la musique
    title = Un champs de texte obligatoire destiner à la saisie le titre de la musique
    year = Un champs de texte obligatoire destiner à la saisie l'année de sortie de la musique
    band = menu deroulant listant tout les nom des groupes presant dans la base de donnée
    genre= checkbox listant tous les genres
    submit= bonton pour valider le formulaire
'''
class MusicFormSans(FlaskForm):
	id = HiddenField('id')
	title = StringField('Titre',validators=[DataRequired()])
	year = IntegerField('Année de publication',validators=[DataRequired()])
	band = SelectField('Groupe',coerce=int, choices=all_band(),validators=[DataRequired()])
	genre = MultiCheckboxField('Genre',coerce=int, choices=all_genre())
	submit = SubmitField("Enregistrer")

'''
Formulaire pour changer une image

Attribut :
    id = Un champs caché prenant l'ID de la musique
    submit= bonton pour valider le formulaire
'''
class ChangerImageForm(FlaskForm):
	id = HiddenField('id')
	fileName = FileField(validators=[FileRequired()])
	submit = SubmitField("Valider")
'''
Formulaire de tri musique

Attribut :
    choix = menu deroulant listant tout les options de tri
    submit= bonton pour valider le formulaire
'''
class MusicTriForm(FlaskForm):
	choix = SelectField('choix',coerce=int, choices=[(1,"annee"),(2,"groupe"),(3,"titre"),(4,"genre"),(5,"favorie")],validators=[DataRequired()])
	submit = SubmitField("Rechercher")

'''
Formulaire de recherche 

Attribut :
    choixR = Un champs de texte obligatoire destiner à la saisie le titre de la musique
    submit= bonton pour valider le formulaire
'''
class MusicRechercheForm(FlaskForm):
	choixR = StringField('Title',validators=[DataRequired()])
	submit = SubmitField("rechercher")


'''
La page d'acceuil du site web
'''
@app.route("/")
def home():
	if (current_user.is_authenticated):
		p=current_user.musics
		g=current_user.groupes
		if len(p)>9:
			p=p[:9]

		if len(g)>9:
			g=g[:9]
		Top=get_music_prefere()
		return render_template(
			"home.html",
			title="Hello ",
			musique=p,
            groupes=g,
			preferer=Top,
			nbP=len(Top),
			home_active="class=active",
			user=current_user,
		)
	return render_template(
		"home.html",
		title="Hello !",
		preferer=get_music_prefere(),
		home_active="class=active")


'''
Route d'affichage du formulaire d'ajout de groupe sans aucune validation du dit formulaire
'''
@app.route("/group/add")
@login_required
def ajouterGroupe():
    f = GroupForm()
    return render_template(
        "Group/add.html",
        title = "Ajout d'un groupe",
        form = f,
    )

'''
Route permettant le traitement des saisie du formulaire d'ajout de groupe ainsi que l'affichage du
formulaire si la validation n'a pas eu lieu
'''
@app.route("/group/addSave", methods=("POST",))
@login_required
def ajout_Groupe():
    g = None
    f = GroupForm()
    if f.validate_on_submit():
        g = Band(bandName = f.name.data,
                 artist = f.artist.data,
                 artist_id = f.artist.data.id)
        db.session.add(g)
        db.session.commit()
        return redirect(url_for('group'))
    return render_template(
        "Group/add.html",
        title = "Ajout d'un groupe",
        form = f 
    )

'''
La page de la liste de tout les groupes
'''
@app.route("/group")
def group():
    if (current_user.is_authenticated):
        return render_template(
            "Group/show.html",
            title="Liste des groupes",
            group_active="class=active",
            group=getAllGroup(),
            fav = current_user.groupes
        )
    else:
        return render_template(
            "Group/show.html",
            title="Liste des groupes",
            group_active="class=active",
            group=getAllGroup(),
        )

'''
Route permettant l'édition d'un groupe en particulier
'''
@app.route("/group/modif/<int:id>")
@login_required
def modifierGroupe(id):
    g = getGroup(id)
    f = GroupForm(id = g.id, name = g.bandName, artist = g.artist)
    return render_template(
        "Group/modif.html",
        group = g,
        form = f,
        title = "Modifier groupe",
    )

'''
Route permettant le sauvegarde dans la base de donnees de la modification d'un groupe
'''
@app.route("/group/modifSave/", methods=("POST",))
@login_required
def save_modifgroup():
    g = None
    f = GroupForm()
    if f.validate_on_submit():
        id = int(f.id.data)
        g = getGroup(id)
        g.bandName = f.name.data
        g.artist = f.artist.data
        db.session.commit()
        return redirect(url_for("modifierGroupe", id = g.id))
    g = getGroup(int(f.id.data))
    return render_template(
        "Group/modif.html",
        group = g,
        form = g,
    )

'''
Route permettant la suppression d'un groupe en particulier
'''
@app.route("/group/delete/<int:id>")
@login_required
def deletegroup(id):
    g = getGroup(id)
    f = GroupFormDelete(id = g.id)
    return render_template(
        "Group/delete.html",
        group = g,
        form = f,
        title = "Supprimer groupe",
    ) 

'''
Route permettant le sauvegarde dans la base de donnees de la suppression d'un groupe
'''
@app.route("/group/deleteSave/", methods=("POST",))
@login_required
def save_deletegroup():
    g = None
    f = GroupFormDelete()
    if f.validate_on_submit():
        id = int(f.id.data)
        g = getGroup(id)
        removeGroup(id)
        db.session.commit()
        return redirect(url_for("group"))
    return render_template(
        "Group/delete.html",
        group = g,
        form = g,
        title = "Supprimer groupe",
    ) 

'''
Route permettant à l'utilisateur de ce connecter avec son compte
'''
@app.route("/login/", methods = ("GET", "POST",))
def login():
    f = LoginForm()
    if not f.is_submitted():
        f.next.data = request.args.get("next")
    elif f.validate_on_submit():
        user = f.get_authenticated_user()
        if user:
            login_user(user)
            next = f.next.data or url_for("home")
            return redirect(next)
    return render_template(
        "login.html",
        form = f
    )


'''
Route permettant d'ajouter un groupe en favorie
'''
@app.route("/group/favorie/add/<int:id>")
@login_required
def ajoutFav(id):
    current_user.groupes.append(getGroup(id))
    db.session.commit()
    return redirect(url_for('group'))

'''
Route permettant de retirer un groupe des favories
'''
@app.route("/group/favorie/delete/<int:id>")
@login_required
def retirerFav(id):
    if id not in current_user.groupes:
        current_user.groupes.remove(getGroup(id))
        db.session.commit()
    return redirect(url_for('group'))

'''
Route permettant que l'utilisateur puisse ce deconnecter
'''
@app.route("/logout/")
def logout():
    logout_user()
    return redirect(url_for('home'))


def testImages(mon):
	if mon==None:
		return "f"
	return "v"
'''
Route d'affichage du formulaire d'ajout d'une musique
'''
@app.route("/music/add/")
@login_required
def add_music():
	f = MusicForm()
	f.band.choices=all_band()
	f.process()
	return render_template(
	"Music/addMusic.html",
	form=f,
	title="Ajout Music",
	)


'''
Route qui enregistre l'ajout d'une musique 
'''
@app.route("/music/save/",methods=("POST",))
@login_required
def save_music():
	f = MusicForm()
	if f.validate_on_submit():
		k = f.fileName.data
		filename = secure_filename(k.filename)
		m = Music(
            title=f.title.data,
            year=f.year.data,
            img=filename,
            band_id=f.band.data
            )
		k.save(mkpath('../rockproject/static/img/'+filename))
		for g in f.genre.data:
			m.genres.append(get_genre(g))
		db.session.add(m)
		db.session.commit()
		return redirect(url_for('affiche_music'))
	return render_template(
	"Music/addMusic.html",
	form=f,
	title="Ajout Music" 
	)

'''
Route qui edite une musique avec le formulaire 
'''	
@app.route("/music/edit/<int:id>",methods=("GET","POST"))
@login_required
def edit_music(id):
	m = get_music(id)
	liste=[]
	for i in m.genres:
		liste.append(i.id)	
	f = MusicFormSans()
	f.id.default = m.id
	f.title.default = m.title
	f.year.default = m.year
	f.band.default = m.band_id
	f.genre.default = liste
	f.band.choices=all_band()
	f.process()
	return render_template(
	"Music/edit_music.html",
	musique=m,
	form=f,
	testImage=testImages(m.img),
	title="Edit Music",
	)

'''
Route qui enregistre les modifications d'une musique 
'''	
@app.route("/music/save/edit/",methods=("POST",))
@login_required
def edit_save_music():
	m = None
	f = MusicFormSans()
	if f.validate_on_submit():
		id = int(f.id.data)
		m=get_music(id)
		m.title = f.title.data
		m.year = f.year.data
		m.band_id = f.band.data
		genres=m.genres
		genresNew=f.genre.data
		for g in genres:
			if g.id not in genresNew:
				m.genres.remove(g)
		for g in genresNew:
			gNew=get_genre(g)
			if gNew not in genres:
				m.genres.append(gNew)
		db.session.commit()
		return redirect(url_for('affiche_music'))
	m=get_music(int(f.id.data))
	return render_template(
	"Music/edit_music.html",
	musique=m,
	form=f,
	testImage=testImages(m.img)
	)
'''
Route permettant changer l'image d'une musique
'''	
@app.route("/image/changer/<int:id>",methods=("POST","GET"))
@login_required
def changer_image(id):
	m=get_music(id)
	f = ChangerImageForm(id=id)
	return render_template(
	"Music/changer_image.html",
	musique=m,
	form=f,
	)
	
'''
Route qui enregistre la nouvelle image de la musique 
'''	
@app.route("/image/save/",methods=("POST","GET"))
@login_required
def edit_changer_image():
	f = ChangerImageForm()
	if f.validate_on_submit():
		m=get_music(int(f.id.data))
		k = f.fileName.data
		filename = secure_filename(k.filename)
		k.save(mkpath('../rockproject/static/img/'+filename))
		m.img=filename
		db.session.commit()
		return redirect(url_for('edit_music', id =int(f.id.data)))
	return redirect(url_for('changer_image', id = f.id.data))
	

'''
Route qui affiche toutes les musiques selon des options
'''
@app.route("/music/affiche/",methods=("POST","GET"))
def affiche_music():
	f = MusicTriForm()
	f2= MusicRechercheForm()
	dico={0:"all_music()",1:"tri_year_music()",2:"tri_band_music()",3:"tri_title_music()",4:"tri_genre_music()",5:"current_user.musics"}
	if (current_user.is_authenticated):
		m=eval(dico[0])
		if f2.validate_on_submit():
			mu=get_un_musique(f2.choixR.data)
			if mu!=0:
				return redirect(url_for('show_music',id = mu.id))
		if f.validate_on_submit():
			m=eval(dico[f.choix.data])
		return render_template(
			"Music/affiche.html",
			musique=m,
			music_active="class=active",
			form=f,
			form1=f2,
			title="Affiche Music",
			fav = current_user.musics,
		)
	else:
		return render_template(
		"Music/affiche.html",
		musique=all_music(),
		form=f,
		form1=f2,
		title="Affiche Music",
		)

'''
Route supprime une musique 
'''
@app.route("/music/delete/<int:id>")
@login_required
def delete_music(id):
	m =get_music(id)
	db.session.delete(m)
	db.session.commit()
	return redirect(url_for('affiche_music'))


'''
Route qui affiche une musique
'''
@app.route("/music/show/<int:id>")
def show_music(id):
	m=get_music(id)
	print(m.img)
	return render_template(
	"Music/music.html",
	musique=m,
	title=m.title,
	testImage=testImages(m.img)
	)

'''
Route permettant d'ajouter une musique en favorie
'''
@app.route("/favorie/music/add/<int:id>")
@login_required
def ajoutFavMusic(id):
    current_user.musics.append(get_music(id))
    db.session.commit()
    return redirect(url_for('affiche_music'))


'''
Route permettant de supprimer une musique en favorie
'''
@app.route("/favorie/music/delete/<int:id>")
@login_required
def deleteFavMusic(id):
    if id not in current_user.groupes:
        current_user.musics.remove(get_music(id))
        db.session.commit()
    return redirect(url_for('affiche_music'))

'''
La page de la liste de tout les artistes
'''
@app.route("/artist")
def artist():
    return render_template(
        "Artist/show.html",
        title="Liste des artistes",
        artist_active="class=active",
        artists=getAllArtist(),
    )

'''
Route permettant l'édition d'un artiste en particulier
'''
@app.route("/artist/modif/<int:id>")
@login_required
def modifierArtiste(id):
    a = getArtist(id)
    f = ArtistForm(id = a.id, name = a.name)
    return render_template(
        "Artist/modif.html",
        artist = a,
        form = f,
        title = "Modifier artiste",
    )

'''
Route permettant le sauvegarde dans la base de donnees de la modification d'un artiste
'''
@app.route("/artist/modifSave/", methods=("POST",))
@login_required
def save_modifartiste():
    a = None
    f = ArtistForm()
    if f.validate_on_submit():
        id = int(f.id.data)
        a = getArtist(id)
        a.name = f.name.data
        db.session.commit()
        return redirect(url_for("modifierArtiste", id = a.id))
    a = getGroup(int(f.id.data))
    return render_template(
        "Artist/modif.html",
        group = a,
        form = a,
    )

'''
Route permettant la suppression d'un artiste en particulier
'''
@app.route("/artiste/delete/<int:id>")
@login_required
def deleteartiste(id):
    a = getArtist(id)
    f = ArtistFormDelete(id = a.id)
    return render_template(
        "Artist/delete.html",
        artist = a,
        form = f,
        title = "Supprimer artiste",
    ) 

'''
Route permettant le sauvegarde dans la base de donnees de la suppression d'un artiste
'''
@app.route("/artiste/deleteSave/", methods=("POST",))
@login_required
def save_deleteartiste():
    a = None
    f = ArtistFormDelete()
    if f.validate_on_submit():
        id = int(f.id.data)
        g = getArtist(id)
        removeArtist(id)
        db.session.commit()
        return redirect(url_for("artist"))
    return render_template(
        "Artist/delete.html",
        artist = a,
        form = a,
        title = "Supprimer artiste",
    ) 


'''
Route d'affichage du formulaire d'ajout d'un artiste
'''
@app.route("/artist/add")
@login_required
def ajouterArtiste():
    f = ArtistForm()
    return render_template(
        "Artist/add.html",
        title = "Ajout d'un artiste",
        form = f,
    )

'''
Route permettant le traitement des saisie du formulaire d'ajout d'un artiste ainsi que l'affichage du
formulaire si la validation n'a pas eu lieu
'''
@app.route("/artist/addSave", methods=("POST",))
@login_required
def ajout_Artiste():
    a = None
    f = ArtistForm()
    if f.validate_on_submit():
        a = Artist(name = f.name.data)
        db.session.add(a)
        db.session.commit()
        return redirect(url_for('artist'))
    return render_template(
        "Artist/add.html",
        title = "Ajout d'un artiste",
        form = f 
    )



